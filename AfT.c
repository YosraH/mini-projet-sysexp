#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
int main (int argc, char **argv){
if(argc < 3){
fprintf(stderr, "Format: AfT file_name chain_to_add\n");
exit(EXIT_FAILURE);
}
else{
FILE *fic2= fopen(argv[1], "a+");
char mot[2024];
int i;
strcpy(mot,"");
for (i=2;i<argc;i++){
strcat(mot,argv[i]);
strcat(mot," ");
}
fputs(mot,fic2);
fclose(fic2);
}
return 0;

}
