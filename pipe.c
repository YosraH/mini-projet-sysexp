#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
int main(int argc, char *argv[]){

	int i;
	char str[1024], cmd1[60], cmd2[50];
	char *token;
if (argc==1){
fprintf(stderr, "Format pipe: ./pipe commande '|' commande\n");
}
         else{
strcpy(str,argv[1]);   
	for(i = 2; i < argc; i++){
strcat(str, " ");
		strcat(str, argv[i]);
		//strcat(str, " ");
	}
//printf("%s",str);
	if(strstr(str, "|") != NULL && strstr(argv[1], "|") == NULL && strstr(argv[i -1], "|") == NULL){
		token = strtok(str, "|");
		strcpy(cmd1, token);
		while(token != NULL){
			strcpy(cmd2, token);
			token = strtok(NULL, "|");
		}
//printf("%s ff %s",cmd1,cmd2);

		pid_t pid;
		int pipefd[2];
		pipe(pipefd);
		pid = fork();
		if(pid == -1){
			perror("pipe failed\n");
			exit(1);
		}
	else	if(pid > 0){ // child process: command "|"
			close(pipefd[0]);
			dup2(pipefd[1], 1);
			system(cmd1);

		}else{ //parent process: "|" command
			close(pipefd[1]);
			dup2(pipefd[0], 0);
			system(cmd2);
		}
	}
	else{
		fprintf(stderr, "Format pipe: ./pipe commande '|' commande\n");
	} 
}
exit(1);
}
