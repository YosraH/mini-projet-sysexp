#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
void trim(char *str){

int index = 0, i;
while(str[index] == ' ' || str[index] == '\t' || str[index] == '\n'){
        index++;
}
i = 0;
while (str[i + index] != '\0'){
        str[i] = str[i + index];
        i++;
}
str[i] = '\0';
i = 0;
index = -1;
while (str[i] != '\0'){
     if(str[i] != ' ' && str[i] != '\t' && str[i] != '\n'){
                index = i;
     }
     i++;
}
str[index + 1] = '\0';
}


int is_direc(const char *path)
{
struct stat path_stat;
stat(path, &path_stat);
return S_ISDIR(path_stat.st_mode);
}
int main (int argc, char **argv){
char rep[50];
if(argc == 1){
fprintf(stderr, "Format: suppdos nom_des_fichiers\n");
exit(EXIT_FAILURE);
}
else{
int a;
for(a=1;a<argc;a++)
{
if(is_direc(argv[a])!=1)
{
fprintf(stderr, "%s is a File Or Not Found\n",argv[a]);
exit(EXIT_FAILURE);
}
else{
printf("remove %s ?",argv[a]);
fgets(rep,50,stdin);
trim(rep);
if(strcmp(rep,"y")==0 || strcmp(rep,"Y")==0 || strcmp(rep,"yes")==0 || strcmp(rep,"YES")==0){
rmdir(argv[a]);}
}}}

return 0;
}
