#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <gtk/gtk.h>
#define BUFSIZE 512
int taille_tab(){
int i=0;
FILE *fic= fopen("/root/MiniprojSys/fileprocess", "r");
signed char texte[256];
while (fgets(texte, 255, fic)!=NULL){
i++;
}
return i;
}
typedef struct Process p;
struct Process
{
   	char nom[200];
   	int darr;
	int tpexec;
	int dd;
	int df;
	int ex;};
void selection(p t[], int taille)
{
     int i, mini, j;
p x;

     for (i = 0; i < taille - 1; i++)
     {
         mini = i;
         for (j = i + 1; j < taille; j++)
              if (t[j].darr < t[mini].darr)
                  mini = j;
          x = t[i];
          t[i] = t[mini];
          t[mini] = x;
     }
}

void trim(char *str){

int index = 0, i;
while(str[index] == ' ' || str[index] == '\t' || str[index] == '\n'){
        index++;
}
i = 0;
while (str[i + index] != '\0'){
        str[i] = str[i + index];
        i++;
}
str[i] = '\0';
i = 0;
index = -1;
while (str[i] != '\0'){
     if(str[i] != ' ' && str[i] != '\t' && str[i] != '\n'){
                index = i;
     }
     i++;
}
str[index + 1] = '\0';
}

void print_gantt_chart(p p[], int n, char gantt[])
{
//char gantt[1024];
    int i, j;
    // print top bar
    //printf(" ");
strcat(gantt,"");
    for(i=0; i<n; i++) {
        for(j=0; j<=(p[i].tpexec); j++) {//printf("--");
strcat(gantt,"--");}
        //printf(" ");
strcat(gantt,"   ");
    }
    //printf("\n|");
strcat(gantt,"\n|");

   // printing process id in the middle
    for(i=0; i<n; i++) {
        for(j=0; j<=(p[i].tpexec) - 1; j++) {//printf(" ");
strcat(gantt," ");}
trim (p[i].nom);        
char nom[100];
strcpy(nom,p[i].nom);
//printf("%s", nom);
strcat(gantt," ");
strcat(gantt,p[i].nom);
strcat(gantt," ");
        for(j=0; j<(p[i].tpexec) - 1; j++) {//printf(" ");
strcat(gantt," ");
}
        //printf("|");

//strcat(gantt," ");
strcat(gantt,"|");

    }
    //printf("\n ");
strcat(gantt,"\n");
    // printing bottom bar
    for(i=0; i<n; i++) {
        for(j=0; j<=(p[i].tpexec); j++){ //printf("--");
strcat(gantt,"--");}
        //printf(" ");
strcat(gantt,"   ");
    }

    //printf("\n");
strcat(gantt,"\n");
    // printing the time line
   //printf("%d",p[0].dd);
char dd[100];
sprintf(dd,"%d",p[0].dd);
//strcat(gantt," ");
strcat(gantt,dd);
//strcat(gantt," ");
    for(i=0; i<n; i++) {
if(p[i].df==p[i+1].dd || i==n-1){
        for(j=0; j<=p[i].tpexec; j++) {
 if(p[i].df > 9) strcat(gantt,"  ");
//printf("  ");
else strcat(gantt,"   ");
}
        
// backspace : remove 1 space
//printf("%d",p[i].df);
sprintf(dd,"%d",p[i].df);
strcat(gantt,dd);
strcat(gantt," ");
    }
else{
for(j=0; j<=(p[i].tpexec); j++) {
 if(p[i].df > 9) strcat(gantt," ");
//printf("  ");
else strcat(gantt,"  ");
}
if(i==0){
//printf(" %d %d",p[i].df,p[i+1].dd);
strcat(gantt," ");
sprintf(dd,"%d",p[i].df);
strcat(gantt,dd);
strcat(gantt," ");
sprintf(dd,"%d",p[i+1].dd);
strcat(gantt,dd);
}
else
{//printf("%d %d",p[i].df,p[i+1].dd);
//strcat((gantt,p[i].df,p[i+1].dd));
sprintf(dd,"%d",p[i].df);
strcat(gantt,dd);
strcat(gantt,"  ");
sprintf(dd,"%d",p[i+1].dd);
strcat(gantt,dd);
}
}
}
    //printf("\n");
strcat(gantt,"\n");

}


void print_table(char g[])
{
p tabp[100];
char var[500];
char buf[BUFSIZE];
int n = taille_tab();
char *cmd="cut -d ' ' -f 1 /root/MiniprojSys/fileprocess";
FILE *fp;
int m=0;
fp = popen(cmd, "r");
	while (fgets(buf, BUFSIZE, fp)!=NULL){
if(strstr(buf,"Nom_du_processus")!=NULL){
continue;}
else
{
strcpy(tabp[m].nom,buf);
m++;
}
}
pclose(fp);
int o=0;
char buf2[BUFSIZE];
char *cmd2="cut -d ' ' -f 2 /root/MiniprojSys/fileprocess";
FILE *fp2;
char str1[500];
fp2 = popen(cmd2, "r");
        while (fgets(buf2, BUFSIZE, fp2)!=NULL){
if(strstr(buf2,"date_d'arrivé")!=NULL){
continue;}
else
{
strcpy(str1,buf2);
tabp[o].darr=atoi(str1);
o++;
}
}
pclose(fp2);
int q=0;
char buf3[BUFSIZE];
char *cmd3="cut -d ' ' -f 3 /root/MiniprojSys/fileprocess";
FILE *fp3;
char str[500];
fp3 = popen(cmd3, "r");
        while (fgets(buf3, BUFSIZE, fp3)!=NULL){
if(strstr(buf3,"temps_d'execution")!=NULL){
continue;}
else
{
strcpy(str,buf3);
tabp[q].tpexec=atoi(str);
q++;
}
}
pclose(fp3);

char nom[500];
int i;
char dd[500];
sprintf(g,"%s","");
strcat(g,"\t\t\t\t");
    strcat(g,"+-----+----------------------+--------------+-------------+");
	strcat(g,"\n");strcat(g,"\t\t\t\t");
    strcat(g,"| PID | Date D'arrivée | Temps D'exercution |");
	strcat(g,"\n");strcat(g,"\t\t\t\t");
    strcat(g,"+-----+----------------------+--------------+-------------+");
	strcat(g,"\n");

    for(i=0; i<4; i++) {
strcat(g,"\t\t\t\t");
trim(tabp[i].nom);
strcat(g,"| ");

strcpy(nom,tabp[i].nom);
strcat(g,nom);
strcat(g," |");
strcat(g,"          ");

sprintf(dd,"%d",tabp[i].darr);
strcat(g,dd);
strcat(g,"           |            ");
sprintf(dd,"%d",tabp[i].tpexec);
strcat(g,dd);
strcat(g,"                 |");

strcat(g,"\n");}
  /*printf("| %2s |      %2d        |         %2d         |    %2d    |\n"
               , tabp[i].nom, tabp[i].darr, tabp[i].tpexec, tabp[i].pr );*/
strcat(g,"\t\t\t\t");
    strcat(g,"+-----+----------------------+--------------+-------------+");


strcat(g,"\n");
strcat(g,"\n");
p tabp2[100];

int j;
int indic=0; 
	p min=tabp[0];
			for(j=1;j<n-1;j++) {
				if (tabp[j].darr==min.darr) {
					if (tabp[j].tpexec<min.tpexec) {
						min=tabp[j];
						indic=j;
					}
				}
				else 
					break;
			}
			tabp2[indic].ex=1;
min.ex=1;			
tabp2[0]=min;
			tabp2[0].dd=min.darr;
			tabp2[0].df=min.darr+min.tpexec;
//printf("yes %d",tabp2[indic].ex);
int k=0;
p min1;
while(k<(5-2)){
tabp[indic].ex=1;
int l;

//printf("yes %d",tabp2[indic].ex);
	l=0;
	min1=tabp[l];
	//printf("%s\n",min1.nom);
	int df=tabp2[k].df;
for(i=1;i<n-1;i++) {

				//if(l<=tabp.length) {
				 if(tabp[l].ex==1) {
					l++;
					min1=tabp[l];
				}
				if(tabp[i].ex==1) {	
continue;
}

				//else {
				if((tabp2[k].df >= tabp[i].darr) && (tabp2[k].df>= tabp[l].darr)) {
					if(tabp[i].tpexec<min1.tpexec) {
						min1=tabp[i];
l=i;
		}
					
			}

 else if (tabp[i].darr== min1.darr) {
                                        if(tabp[i].tpexec<min1.tpexec) {
                                                min1=tabp[i];
//l=i;                          
}
                                        df=min1.darr;


                        }
                                else if  ((tabp[i].darr> min1.darr)){
                                        df=min1.darr;

                                }

				
				
				}
             		tabp[l].ex=1;
tabp[indic].ex=1;
min1.ex=1;		
k++;
			tabp2[k]=min1;
			tabp2[k].dd=df;
			tabp2[k].df=min1.tpexec+df;
			
}
   char dk[1024];
for(i=0;i<=k;i++){
trim(tabp2[i].nom);
strcpy(nom,tabp2[i].nom);
strcat(g,"** Nom Du processus:    ");
strcat(g,nom);
strcat(g,"\t\t");
sprintf(dk,"%d",tabp2[i].dd);
strcat(g,"** Debut D'Execution:     ");
strcat(g,dk);
if(tabp2[i].dd>9)
strcat(g,"\t");
else
strcat(g,"\t\t");
sprintf(dk,"%d",tabp2[i].df);
strcat(g,"** Fin D'Execution:     ");
strcat(g,dk);
strcat(g,"\n");
} 
} 
void greet( GtkWidget *widget, gpointer   data)
{
	gchar* sUtf8;
gchar res[2048];
GtkWidget* p_Window;
char ss [2048];
print_table(ss);
strcpy(res,ss);
sUtf8 = g_locale_to_utf8(res, -1, NULL, NULL, NULL);

  //gtk_window_set_default_size(GTK_WINDOW(p_Window), 1000, 300);
p_Window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(p_Window), "MORE DETAILS (SJF)");
    gtk_window_set_default_size(GTK_WINDOW(p_Window), 1024, 800);
	gtk_window_set_position (GTK_WINDOW (p_Window), GTK_WIN_POS_CENTER);
	g_signal_connect(G_OBJECT(p_Window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
const char *format = "<span foreground=\"#03224C\" face=\"Time New Roman\">\%s</span>";
char *markup;

markup = g_markup_printf_escaped (format, sUtf8);

GtkWidget *label = gtk_label_new (NULL);
gtk_label_set_markup (GTK_LABEL (label), markup);
g_free (markup);
gtk_container_add(GTK_CONTAINER(p_Window), label);
gtk_widget_show_all(p_Window);

    gtk_main();
 
}

 
int main(int argc, char *argv[]){
p tabp[100];
p tabp2[100];
char var[500];
char buf[BUFSIZE];
int n = taille_tab();
char *cmd="cut -d ' ' -f 1 /root/MiniprojSys/fileprocess";
FILE *fp;
int m=0;
fp = popen(cmd, "r");
	while (fgets(buf, BUFSIZE, fp)!=NULL){
if(strstr(buf,"Nom_du_processus")!=NULL){
continue;}
else
{
strcpy(tabp[m].nom,buf);
m++;
}
}
pclose(fp);
int o=0;
char buf2[BUFSIZE];
char *cmd2="cut -d ' ' -f 2 /root/MiniprojSys/fileprocess";
FILE *fp2;
char str1[500];
fp2 = popen(cmd2, "r");
        while (fgets(buf2, BUFSIZE, fp2)!=NULL){
if(strstr(buf2,"date_d'arrivé")!=NULL){
continue;}
else
{
strcpy(str1,buf2);
tabp[o].darr=atoi(str1);
o++;
}
}
pclose(fp2);
int q=0;
char buf3[BUFSIZE];
char *cmd3="cut -d ' ' -f 3 /root/MiniprojSys/fileprocess";
FILE *fp3;
char str[500];
fp3 = popen(cmd3, "r");
        while (fgets(buf3, BUFSIZE, fp3)!=NULL){
if(strstr(buf3,"temps_d'execution")!=NULL){
continue;}
else
{
strcpy(str,buf3);
tabp[q].tpexec=atoi(str);
q++;
}
}
pclose(fp3);
/*for (j;j<n-1;j++){
//printf("%s %d %d",tabp[j].nom,tabp[j].darr,tabp[j].tpexec);
if(tabp[j].tpexec>tabp[j+1].tpexec)
printf("%d>%d",tabp[j].tpexec,tabp[j+1].tpexec);
}*/
//selection(tabp,n-1);
/*int f=0;
for (f;f<n-1;f++){
printf("%s %d %d",tabp[f].nom,tabp[f].darr,tabp[f].tpexec);
//if(tabp[j].tpexec>tabp[j+1].tpexec)
//printf("%d>%d",tabp[j].tpexec,tabp[j+1].tpexec);
}*/
int j;
int indic=0; 
	p min=tabp[0];
			for(j=1;j<n-1;j++) {
				if (tabp[j].darr==min.darr) {
					if (tabp[j].tpexec<min.tpexec) {
						min=tabp[j];
						indic=j;
					}
				}
				else 
					break;
			}
			tabp2[indic].ex=1;
min.ex=1;			
tabp2[0]=min;
			tabp2[0].dd=min.darr;
			tabp2[0].df=min.darr+min.tpexec;
//printf("yes %d",tabp2[indic].ex);
int k=0;
p min1;
while(k<n-2){
tabp[indic].ex=1;
int l;
int i=1;
//printf("yes %d",tabp2[indic].ex);
	l=0;
	min1=tabp[l];
	//printf("%s\n",min1.nom);
	int df=tabp2[k].df;
for(i=1;i<n-1;i++) {

				//if(l<=tabp.length) {
				 if(tabp[l].ex==1) {
					l++;
					min1=tabp[l];
				}
				if(tabp[i].ex==1) {	
continue;
}

				//else {
				if((tabp2[k].df >= tabp[i].darr) && (tabp2[k].df>= tabp[l].darr)) {
					if(tabp[i].tpexec<min1.tpexec) {
						min1=tabp[i];
l=i;
		}
					
			}

 else if (tabp[i].darr== min1.darr) {
                                        if(tabp[i].tpexec<min1.tpexec) {
                                                min1=tabp[i];
//l=i;                          
}
                                        df=min1.darr;


                        }
                               /* else if  ((tabp[i].darr> min1.darr)){
                                        df=min1.darr;

                                }*/

				
				
				}
             		tabp[l].ex=1;
tabp[indic].ex=1;
min1.ex=1;		
k++;
			tabp2[k]=min1;
			tabp2[k].dd=df;
			tabp2[k].df=min1.tpexec+df;
			
}
int g;
double ttm;
double tam;
//printf("\033[1;34m");
for (g=0;g<n-1;g++){
ttm=ttm+(tabp2[g].df-tabp2[g].darr);
tam=tam+((tabp2[g].df-tabp2[g].darr)-tabp2[g].tpexec);
}
//printf("\033[0m");
/*int f=0;
  for (f;f<n-1;f++){
 printf("%d ",tabp[f].tpexec);
 //if(tabp[j].tpexec>tabp[j+1].tpexec)
//printf("%d>%d",tabp[j].tpexec,tabp[j+1].tpexec)
}*/

GtkWidget* p_Window;
    GtkWidget* p_Label;
	gchar* sUtf8;

	gchar res[2048];
GtkWidget *button;

    gtk_init(&argc,&argv);

    p_Window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(p_Window), "DIAGRAMM GANTT PRIORITY SJF");
    gtk_window_set_default_size(GTK_WINDOW(p_Window), 600, 300);
	gtk_window_set_position (GTK_WINDOW (p_Window), GTK_WIN_POS_CENTER);
	g_signal_connect(G_OBJECT(p_Window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
 
    
    //p_Label=gtk_label_new(sUtf8);
   //g_free(sUtf8);
char ss [1028];
char ttm2[1024];
char tam2[2024];
sprintf(ttm2,"%-2.2lf",ttm/(n-1));
sprintf(tam2,"%-2.2lf\n",tam/(n-1));
print_gantt_chart(tabp2,n-1,ss);
strcat(ss,"\n");
strcat(ss,"TTM:  "),
strcat(ss,ttm2);
strcat(ss,"\n");
strcat(ss,"TAM:  "),
strcat(ss,tam2);
strcat(ss,"\n");
GtkWidget *hbox = gtk_hbox_new (FALSE, 0);
strcpy(res,ss);
sUtf8 = g_locale_to_utf8(res, -1, NULL, NULL, NULL);
const char *format = "<span foreground=\"#5472AE\" weight=\"bold\">\%s</span>";
char *markup;

markup = g_markup_printf_escaped (format, sUtf8);

GtkWidget *label = gtk_label_new (NULL);
gtk_label_set_markup (GTK_LABEL (label), markup);
g_free (markup);
//char * markup = g_markup_printf_escaped (format, input);
//gtk_label_set_markup (GTK_LABEL (label), markup);
//g_free (markup); 
GtkSettings *default_settings = gtk_settings_get_default();
g_object_set(default_settings, "gtk-button-images", TRUE, NULL);
gtk_container_add (GTK_CONTAINER (p_Window), hbox); 
GtkImage *imagen_pantalla_completa;
button = gtk_button_new_with_label ("");
 imagen_pantalla_completa = (GtkImage *)gtk_image_new_from_file("/tmp/m.png"); 
gtk_button_set_image (GTK_BUTTON(button),(GtkWidget *)imagen_pantalla_completa); 

   g_signal_connect (GTK_OBJECT(button),"clicked",G_CALLBACK (greet),"button");
//gtk_container_add(GTK_CONTAINER(p_Window), label);

//gtk_container_add(GTK_CONTAINER(p_Window), button);
//print_table(tabp,n-1);
gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 100);
gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show_all(p_Window);

    gtk_main();
//printf("%s",res);

    return EXIT_SUCCESS;
}



