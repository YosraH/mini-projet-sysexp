#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int is_regular_file(const char *path)
{
struct stat path_stat;
stat(path, &path_stat);
return S_ISREG(path_stat.st_mode);
}
int main (int argc, char **argv){
if(argc <3){
fprintf(stderr, "Format: ecrase file_name chain_to_ecrase\n");
exit(EXIT_FAILURE);
}
else{
FILE *fic2= fopen(argv[1], "w");
if(is_regular_file(argv[1])==0){
fprintf(stderr, "%s is a directory\n",argv[1]);
exit(EXIT_FAILURE);
}
else{
char mot[500];
strcpy(mot,"");
int i;
for (i=2;i<argc;i++){
strcat(mot,argv[i]);
strcat(mot," ");
}

fputs(mot,fic2);
fclose(fic2);
}}
return 0;
}
