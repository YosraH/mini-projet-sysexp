#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <unistd.h>
#include <time.h>
#include <termios.h>
#include <sys/ioctl.h>
void mode_to_letter(int mode, char *str)
{
str[0] = '-';
// judge file type
if(S_ISDIR(mode)) {str[0] = 'd';}
if(S_ISCHR(mode)) {str[0] = 'c';}
if(S_ISBLK(mode)) {str[0] = 'b';}

// judge permission for owner
 if(mode & S_IRUSR) {str[1] = 'r';}
 else {str[1] = '-';}
 if(mode & S_IWUSR) {str[2] = 'w';}
 else {str[2] = '-';}
 if(mode & S_IXUSR) {str[3] = 'x';}
 else{ str[3] = '-';}

 // judge permission for owner group
 if(mode & S_IRGRP) {str[4] = 'r';}
 else {str[4] = '-';}
 if(mode & S_IWGRP) {str[5] = 'w';}
 else {str[5] = '-';}
 if(mode & S_IXGRP) {str[6] = 'x';}
 else {str[6] = '-';}

 // judge permission for others
 if(mode & S_IROTH) {str[7] = 'r';}
 else {str[7] = '-';}
 if(mode & S_IWOTH) {str[8] = 'w';}
 else {str[8] = '-';}
 if(mode & S_IXOTH) {str[9] = 'x';}
 else {str[9] = '-';}

 str[10] = '\0';
 }
int main(int argc, char *argv[]){
DIR *mydir;
struct dirent *myfile;
struct stat fst;
struct tm lt;
struct passwd *pwd;
struct tm * mytime = (struct tm *) malloc(sizeof(struct tm));
if(argc>2)
{
printf("Format: listll access_path");
}
if(argc==1){
char s[100];
/*pwd*/
getcwd(s,100); 
mydir= opendir(s);
while((myfile= readdir(mydir))!=NULL){
stat(myfile->d_name, &fst);
char str[120];
mode_to_letter(fst.st_mode, str);
printf("%s", str);
    // file hard links
printf(" %d", fst.st_nlink);
  // file's owner
printf(" %s", getpwuid(fst.st_uid)->pw_name);
 // file's owner group
 printf(" %s", getgrgid(fst.st_gid)->gr_name);
  // file size
  printf(" %ld", (long)fst.st_size);
 // file time
 mytime = localtime(&fst.st_mtime);
 printf(" %d-%02d-%02d %02d:%02d", mytime->tm_year + 1900, mytime->tm_mon + 1, mytime->tm_mday, mytime->tm_hour, mytime->tm_min);
   // file nam
   printf(" %s", myfile->d_name);
   printf("\n");
}
/*else if (argc ==1){
char *path=getenv("PATH");
system(path);
}*/
closedir(mydir);
}
if(argc==2){
chdir(argv[1]);
mydir= opendir(argv[1]);
while((myfile= readdir(mydir))!=NULL){
stat(myfile->d_name, &fst);
char str[120];
mode_to_letter(fst.st_mode, str);
printf("%s", str);
    // file hard links
printf(" %d", fst.st_nlink);
  // file's owner
printf(" %s", getpwuid(fst.st_uid)->pw_name);
 // file's owner group
 printf(" %s", getgrgid(fst.st_gid)->gr_name);
  // file size
  printf(" %ld", (long)fst.st_size);
 // file time
 mytime = localtime(&fst.st_mtime);
 printf(" %d-%02d-%02d %02d:%02d", mytime->tm_year + 1900, mytime->tm_mon + 1, mytime->tm_mday, mytime->tm_hour, mytime->tm_min);
   // file nam
   printf(" %s", myfile->d_name);
   printf("\n");
}
/*else if (argc ==1){
char *path=getenv("PATH");
system(path);
}*/
closedir(mydir);


}}

