#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
/*rename*/
int is_regular_file(const char *path)
{
struct stat path_stat;
stat(path, &path_stat);
return S_ISREG(path_stat.st_mode);
}
int main (int argc, char **argv){
if (argc!=3)
{
printf("Format rename : rename file newname");
exit(1);
}
else{
if(is_regular_file(argv[1])==0){
fprintf(stderr, "%s is a Directory Or Not Found\n",argv[1]);
exit(EXIT_FAILURE);
}else{
rename(argv[1], argv[2]);}}
return 0;}

