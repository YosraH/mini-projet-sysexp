#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
int is_regular_file(const char *path)
{
struct stat path_stat;
stat(path, &path_stat);
return S_ISREG(path_stat.st_mode);
}

void trim(char *str){

int index = 0, i;
while(str[index] == ' ' || str[index] == '\t' || str[index] == '\n'){
        index++;
}
i = 0;
while (str[i + index] != '\0'){
        str[i] = str[i + index];
        i++;
}
str[i] = '\0';
i = 0;
index = -1;
while (str[i] != '\0'){
     if(str[i] != ' ' && str[i] != '\t' && str[i] != '\n'){
                index = i;
     }
     i++;
}
str[index + 1] = '\0';
}

int main (int argc, char **argv){
char rep[50];
if (argc ==1){
printf("Format: remove name_of_files\n");
}
else if(argc==2)
{
if(is_regular_file(argv[1])==0){
fprintf(stderr, "%s is a directory or Not Found\n",argv[1]);
exit(EXIT_FAILURE);
}
else {printf("remove %s ?",argv[1]);
fgets(rep,50,stdin);
trim(rep);
if(strcmp(rep,"y")==0 || strcmp(rep,"Y")==0 || strcmp(rep,"yes")==0 || strcmp(rep,"YES")==0){
remove (argv[1]);
}
}}
else if(argc>2){
int i;
for(i=1;i<argc;i++){
if(is_regular_file(argv[i])==0){
fprintf(stderr, "%s is a Directory Or Not Found\n",argv[i]);
exit(EXIT_FAILURE);
}
else{
printf("remove %s ?",argv[i]);
fgets(rep,50,stdin);
trim(rep);
if(strcmp(rep,"y")==0 || strcmp(rep,"Y")==0 || strcmp(rep,"yes")==0 || strcmp(rep,"YES")==0){
remove (argv[i]);
}
}}
}
return 0;
}
